import requests
from time import sleep
from bs4 import BeautifulSoup

print("WARNING this program needs more extensive testing!")


with open('isbnlist.txt') as f:
    isbnList = f.readlines()

data = 0
fails = 0

for isbn in isbnList:
    URL = f"https://catalog.loc.gov/vwebv/search?searchArg= \
        {isbn}&searchCode=GKEY%5E*&searchType=0&recCount=25"
    lcc = False
    name = False
    data += 2

    try:
        response = requests.get(URL)

    except:
        print("Something went wrong with the http request. \
            (Do you have internet?)")
        fails += 2
        continue

    if response.status_code != 200:
        print("There was an error with the http request. \
            This could be a poor server connection.")
        fails += 2
        continue

    soup = BeautifulSoup(response.content, "html.parser")
    for i in soup.findAll('div', 'items-wrapper'):
        string = list(i.children)[1].get_text()

        if string == "LC classification":
            print(list(i.children)[3].span.get_text().strip())
            lcc = True
            break

    if not lcc:
        print("Could not find LCC for ", isbn.strip())
        fails += 1

    for i in soup.findAll('div', 'top-information-content'):

        if list(i.children)[1].get_text() == "Personal name":
            print(list(i.children)[3].get_text().strip())
            name = True
            break

    if not name:
        print("Could not find author for ", isbn.strip())
        fails += 1

    sleep(1)
    print("")

print("Fail rate: ", str(round((fails/data)*100, 2))+"%")
